variable "name" {
  type = list(string)
}

variable "instance_type" {
   type = string
   default = "t2.micro"

}

variable "ami" {
   type = string
   default = "ami-0e0bf4b3a0c0e0adc"  
}

