  module "auto_scaling" {
  source = "../auto_scaling/"
  name = var.name
  image_id = var.image_id
  instance_type = var.instance_type
  key_name = var.key_name   
  security_groups = ["sg-0b14d541"]              
  volume_size = var.volume_size
  max_size = "3"
  min_size = "2"
  desired_capacity = "3"
  vpc_zone_identifier = var.subnets
  target_group_arns = [module.alb.tg]
}
 
module "alb" {
  source = "../alb"
  name = var.name
  vpc_id = var.vpc_id
  security_groups = ["sg-0b14d541"]
  subnets = var.subnets
}
